use managed::ManagedSlice;
use mio::unix::EventedFd;
use mio::{Events, Poll};
use slab::Slab;
use smoltcp::iface::{NeighborAnswer, NeighborCache};
use smoltcp::phy::{Device, DeviceCapabilities, RxToken, TxToken};
use smoltcp::socket::{PollAt, Socket, SocketHandle, SocketSet, TcpState};
use smoltcp::socket::{TcpSocket, TcpSocketBuffer};
use smoltcp::time::Duration;
use smoltcp::time::Instant;
use smoltcp::wire::pretty_print::PrettyPrinter;
use smoltcp::wire::EthernetFrame;
use smoltcp::wire::Icmpv6DstUnreachable;
use smoltcp::wire::NdiscRepr;
use smoltcp::wire::{ArpOperation, ArpPacket, ArpRepr};
use smoltcp::wire::{DhcpMessageType, DhcpPacket, DhcpRepr};
use smoltcp::wire::{EthernetAddress, EthernetProtocol, IpAddress, IpCidr, IpProtocol, IpRepr};
use smoltcp::wire::{Icmpv4DstUnreachable, Icmpv4Packet, Icmpv4Repr};
use smoltcp::wire::{Icmpv6Packet, Icmpv6Repr};
use smoltcp::wire::{Ipv4Address, Ipv4Packet, Ipv4Repr, IPV4_MIN_MTU};
use smoltcp::wire::{Ipv6Repr, IPV6_MIN_MTU};
use smoltcp::wire::{TcpControl, TcpPacket, TcpRepr};
use smoltcp::wire::{UdpPacket, UdpRepr};
use smoltcp::{Error, Result};

use crate::ioctls::get_input_buffer_size;

use std::cmp;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::io::{Read, Write};
use std::net::{TcpStream, UdpSocket};
use std::os::unix::io::{AsRawFd, RawFd};

#[derive(Debug)]
struct SlirpUdp {
    socket: UdpSocket,
    src: (IpAddress, u16),
    dst: (IpAddress, u16),
}

#[derive(Debug)]
struct SlirpTcp {
    socket: TcpStream,
    src: (IpAddress, u16),
    dst: (IpAddress, u16),
    handle: SocketHandle,
}

#[derive(Debug)]
enum SlirpConn {
    Udp(SlirpUdp),
    Tcp(SlirpTcp),
}

impl SlirpConn {
    fn as_mut_tcp(&mut self) -> &mut SlirpTcp {
        match *self {
            SlirpConn::Tcp(ref mut tcp) => tcp,
            _ => panic!(),
        }
    }
    fn as_mut_udp(&mut self) -> &mut SlirpUdp {
        match *self {
            SlirpConn::Udp(ref mut udp) => udp,
            _ => panic!(),
        }
    }
}

struct SlirpInner<'z> {
    poll: Poll,
    conns: Slab<SlirpConn>,
    neighbor_cache: NeighborCache<'z>,
    ip_addrs: ManagedSlice<'z, IpCidr>,
    fwd_dns: Option<IpAddress>,
    device_capabilities: DeviceCapabilities,
    // emulated hosts use the MAC addr 52:55:IP:IP:IP:IP
    special_ethernet_addr: EthernetAddress,
    udp_src_map: HashMap<(IpAddress, u16), usize>,
    tcp_src_map: HashMap<(IpAddress, u16), usize>,
}

#[derive(Debug, PartialEq)]
enum Packet<'a> {
    None,
    Arp(ArpRepr),
    Udp((IpRepr, UdpRepr<'a>)),
    Dhcpv4((Ipv4Repr, DhcpRepr<'a>)),
    Icmpv4((Ipv4Repr, Icmpv4Repr<'a>)),
    Icmpv6((Ipv6Repr, Icmpv6Repr<'a>)),
    Tcp((IpRepr, TcpRepr<'a>)),
}

impl<'a> Packet<'a> {
    fn neighbor_addr(&self) -> Option<IpAddress> {
        match self {
            Packet::None => None,
            Packet::Arp(_) => None,
            Packet::Dhcpv4((ref ipv4_repr, _)) => Some(ipv4_repr.dst_addr.into()),
            Packet::Icmpv4((ref ipv4_repr, _)) => Some(ipv4_repr.dst_addr.into()),
            Packet::Icmpv6((ref ipv6_repr, _)) => Some(ipv6_repr.dst_addr.into()),
            Packet::Udp((ref ip_repr, _)) => Some(ip_repr.dst_addr()),
            Packet::Tcp((ref ip_repr, _)) => Some(ip_repr.dst_addr()),
        }
    }
}

fn icmp_reply_payload_len(len: usize, mtu: usize, header_len: usize) -> usize {
    // Send back as much of the original payload as will fit within
    // the minimum MTU required by IPv4. See RFC 1812 § 4.3.2.3 for
    // more details.
    //
    // Since the entire network layer packet must fit within the minumum
    // MTU supported, the payload must not exceed the following:
    //
    // <min mtu> - IP Header Size * 2 - ICMPv4 DstUnreachable hdr size
    cmp::min(len, mtu - header_len * 2 - 8)
}

fn poll_reregister_fd(poll: &Poll, fd: RawFd, tok: usize, can_send: bool, can_recv: bool) {
    let mut interest = mio::Ready::empty() | mio::unix::UnixReady::hup();

    if can_send {
        interest |= mio::Ready::readable();
    };
    if can_recv {
        interest |= mio::Ready::writable();
    };

    dbg!(&(fd, interest));
    poll.reregister(
        &EventedFd(&fd),
        mio::Token(tok),
        interest,
        mio::PollOpt::level(),
    )
    .unwrap();
}

impl SlirpInner<'_> {
    fn contains_addr<T: Into<IpAddress>>(&self, addr: T) -> bool {
        let addr = addr.into();
        self.ip_addrs.iter().any(|probe| probe.contains_addr(&addr))
    }

    /// Get the first IPv4 address of the interface.
    pub fn ipv4_address(&self) -> Option<Ipv4Address> {
        self.ip_addrs
            .iter()
            .filter_map(|addr| match addr {
                &IpCidr::Ipv4(cidr) => Some(cidr.address()),
                _ => None,
            })
            .next()
    }

    /// Get the first IPv4 netmask of the interface.
    pub fn ipv4_netmask(&self) -> Option<Ipv4Address> {
        self.ip_addrs
            .iter()
            .filter_map(|addr| match addr {
                &IpCidr::Ipv4(cidr) => Some(cidr.netmask()),
                _ => None,
            })
            .next()
    }

    fn process_arp<'frame, T: AsRef<[u8]>>(
        &mut self,
        timestamp: Instant,
        eth_frame: &EthernetFrame<&'frame T>,
    ) -> Result<Packet<'frame>> {
        let arp_packet = ArpPacket::new_checked(eth_frame.payload())?;
        let arp_repr = ArpRepr::parse(&arp_packet)?;

        if let ArpRepr::EthernetIpv4 {
            operation,
            source_hardware_addr,
            source_protocol_addr,
            target_protocol_addr,
            ..
        } = arp_repr
        {
            let fill = |a: &mut Self| {
                a.neighbor_cache.fill(
                    IpAddress::Ipv4(source_protocol_addr),
                    source_hardware_addr,
                    timestamp,
                )
            };

            match operation {
                ArpOperation::Request => {
                    if source_protocol_addr == target_protocol_addr {
                        /* Gratuitous ARP */
                        fill(self);
                    }

                    if self.contains_addr(target_protocol_addr) && self.ipv4_address().is_some() {
                        let server_addr = self.ipv4_address().unwrap();
                        if target_protocol_addr == server_addr {
                            fill(self);

                            return Ok(Packet::Arp(ArpRepr::EthernetIpv4 {
                                operation: ArpOperation::Reply,
                                source_hardware_addr: self.special_ethernet_addr,
                                target_hardware_addr: source_hardware_addr,
                                source_protocol_addr: target_protocol_addr,
                                target_protocol_addr: source_protocol_addr,
                            }));
                        }
                    }
                }
                ArpOperation::Reply => {
                    fill(self);
                }
                _ => (),
            }
        }

        Ok(Packet::None)
    }

    fn process_dhcp<'frame>(
        &self,
        ip_repr: IpRepr,
        udp_repr: UdpRepr<'frame>,
    ) -> Result<Packet<'frame>> {
        let server_ip = match self.ipv4_address() {
            Some(ip) => ip,
            None => return Err(Error::Illegal),
        };

        // FIXME: dynamic allocation
        let mut your_ip = server_ip;
        your_ip.0[3] = 15;

        let dhcp_packet = DhcpPacket::new_checked(udp_repr.payload)?;
        let dhcp_repr = DhcpRepr::parse(&dhcp_packet)?;
        let mut dhcp_reply_repr = DhcpRepr {
            transaction_id: dhcp_repr.transaction_id,
            client_hardware_address: dhcp_repr.client_hardware_address,
            your_ip,
            server_ip,
            subnet_mask: self.ipv4_netmask(),
            router: Some(server_ip),
            server_identifier: Some(server_ip),
            dns_servers: Some([Some(server_ip), None, None]),
            ..Default::default()
        };

        let ipv4_repr = match ip_repr {
            IpRepr::Ipv4(ipv4_repr) => ipv4_repr,
            _ => return Err(Error::Unrecognized),
        };

        match dhcp_repr.message_type {
            DhcpMessageType::Discover => {
                dhcp_reply_repr.message_type = DhcpMessageType::Offer;
                Ok(self.dhcpv4_reply(ipv4_repr, dhcp_reply_repr))
            }
            DhcpMessageType::Request => {
                dhcp_reply_repr.message_type = DhcpMessageType::Ack;
                dhcp_reply_repr.ip_lease_time = Some(24 * 3600);
                //let req_ip = dhcp_repr.requested_ip.unwrap_or(dhcp_repr.client_ip);

                Ok(self.dhcpv4_reply(ipv4_repr, dhcp_reply_repr))
            }
            _ => Err(Error::Unrecognized),
        }
    }

    fn process_udp<'frame>(
        &mut self,
        ip_repr: IpRepr,
        ip_payload: &'frame [u8],
    ) -> Result<Packet<'frame>> {
        let (src_addr, dst_addr) = (ip_repr.src_addr(), ip_repr.dst_addr());
        let udp_packet = UdpPacket::new_checked(ip_payload)?;
        let checksum_caps = self.device_capabilities.checksum.clone();
        let udp_repr = UdpRepr::parse(&udp_packet, &src_addr, &dst_addr, &checksum_caps)?;

        if dst_addr.is_broadcast() && udp_repr.dst_port == 67 {
            return self.process_dhcp(ip_repr, udp_repr);
        }

        let fix_dst_addr =
            if self.contains_addr(dst_addr) && udp_repr.dst_port == 53 && self.fwd_dns.is_some() {
                self.fwd_dns.unwrap()
            } else {
                dst_addr
            };

        let sent = (|| {
            let e = self.udp_src_map.entry((src_addr, udp_repr.src_port));
            let conn = match e {
                Vacant(e) => {
                    fn udp_connect(addr: IpAddress, port: u16) -> std::io::Result<UdpSocket> {
                        let socket = UdpSocket::bind("0.0.0.0:0")?;
                        socket.connect((std::net::IpAddr::from(addr), port))?;
                        Ok(socket)
                    }

                    // FIXME: add timeout
                    let udp = SlirpUdp {
                        socket: udp_connect(fix_dst_addr, udp_repr.dst_port)?,
                        src: (src_addr, udp_repr.src_port),
                        dst: (dst_addr, udp_repr.dst_port),
                    };

                    let fd = udp.socket.as_raw_fd();
                    let tok = self.conns.insert(SlirpConn::Udp(udp));
                    self.poll
                        .register(
                            &EventedFd(&fd),
                            mio::Token(tok),
                            mio::Ready::readable()
                                | mio::unix::UnixReady::hup()
                                | mio::unix::UnixReady::error(),
                            mio::PollOpt::level(),
                        )
                        .unwrap();
                    e.insert(tok);
                    &mut self.conns[tok]
                }
                Occupied(e) => &mut self.conns[*e.get()],
            };

            if let SlirpConn::Udp(udp) = conn {
                udp.socket.send(udp_repr.payload)
            } else {
                panic!()
            }
        })()
        .map_err(|e| {
            dbg!(&e);
            e
        })
        .is_ok();

        if sent {
            return Ok(Packet::None);
        }

        self.udp_src_map.remove(&(src_addr, udp_repr.src_port));

        // The packet wasn't handled by a socket, send an ICMP port unreachable packet.
        match ip_repr {
            IpRepr::Ipv4(ipv4_repr) => {
                let payload_len =
                    icmp_reply_payload_len(ip_payload.len(), IPV4_MIN_MTU, ipv4_repr.buffer_len());
                let icmpv4_reply_repr = Icmpv4Repr::DstUnreachable {
                    reason: Icmpv4DstUnreachable::PortUnreachable,
                    header: ipv4_repr,
                    data: &ip_payload[0..payload_len],
                };
                Ok(self.icmpv4_reply(ipv4_repr, icmpv4_reply_repr))
            }
            IpRepr::Ipv6(ipv6_repr) => {
                let payload_len =
                    icmp_reply_payload_len(ip_payload.len(), IPV6_MIN_MTU, ipv6_repr.buffer_len());
                let icmpv6_reply_repr = Icmpv6Repr::DstUnreachable {
                    reason: Icmpv6DstUnreachable::PortUnreachable,
                    header: ipv6_repr,
                    data: &ip_payload[0..payload_len],
                };
                Ok(self.icmpv6_reply(ipv6_repr, icmpv6_reply_repr))
            }
            IpRepr::Unspecified { .. } | IpRepr::__Nonexhaustive => Err(Error::Unaddressable),
        }
    }

    fn conn_readable_udp<F>(&mut self, tok: usize, emit: F) -> Result<()>
    where
        F: FnOnce(&mut Self, (IpRepr, UdpRepr)) -> Result<()>,
    {
        let udp = self.conns[tok].as_mut_udp();
        let len = match get_input_buffer_size(udp.socket.as_raw_fd()) {
            Ok(n) => n,
            Err(_) => return Err(Error::Dropped),
        };

        let mut buffer = vec![0; len];
        match udp.socket.recv(&mut buffer) {
            Ok(received) => {
                let repr = UdpRepr {
                    src_port: udp.dst.1,
                    dst_port: udp.src.1,
                    payload: &buffer[..received],
                };
                let ip_repr = IpRepr::Unspecified {
                    src_addr: udp.dst.0,
                    dst_addr: udp.src.0,
                    protocol: IpProtocol::Udp,
                    payload_len: repr.buffer_len(),
                    hop_limit: 64,
                };
                return emit(self, (ip_repr, repr));
            }
            Err(e) => {
                dbg!(e);
                unimplemented!();
            }
        }
    }

    fn conn_writable_tcp(&mut self, sockets: &mut SocketSet, tok: usize) -> Result<()> {
        let tcp = self.conns[tok].as_mut_tcp();
        let mut socket = sockets.get::<TcpSocket>(tcp.handle);

        dbg!(socket.state());
        //if socket.state() < TcpState::Established {
        //    dbg!(1);
        //}
        let res = socket.recv(|buf| match tcp.socket.write(buf) {
            Ok(n) => (n, Ok(())),
            Err(e) => (0, Err(e)),
        });
        dbg!(&res);
        let res = res?;
        res.map_err(|e| {
            dbg!(e);
            Error::Exhausted
        })?;

        let can_send = socket.can_send();
        let can_recv = socket.can_recv();

        if !can_recv {
            Self::conn_reregister(&self.poll, &tcp.socket, tok, can_send, can_recv);
        }
        Ok(())
    }

    fn conn_readable_tcp(&mut self, sockets: &mut SocketSet, tok: usize) -> Result<()> {
        let tcp = self.conns[tok].as_mut_tcp();
        let mut socket = sockets.get::<TcpSocket>(tcp.handle);

        let res = socket.send(|buf| match tcp.socket.read(buf) {
            Ok(n) => (n, Ok(())),
            Err(e) => (0, Err(e)),
        });
        dbg!(&res);
        let res = res?;
        res.map_err(|e| {
            dbg!(e);
            Error::Exhausted
        })?;

        let can_send = socket.can_send();
        let can_recv = socket.can_recv();

        if !can_send {
            Self::conn_reregister(&self.poll, &tcp.socket, tok, can_send, can_recv);
        }
        Ok(())
    }

    fn conn_hup_tcp(&mut self, sockets: &mut SocketSet, tok: usize) -> Result<()> {
        let tcp = self.conns[tok].as_mut_tcp();
        let mut socket = sockets.get::<TcpSocket>(tcp.handle);

        socket.close();
        self.poll
            .deregister(&EventedFd(&tcp.socket.as_raw_fd()))
            .unwrap();

        Ok(())
    }

    fn conn_reregister<T: AsRawFd>(
        poll: &Poll,
        fd: &T,
        tok: usize,
        can_send: bool,
        can_recv: bool,
    ) {
        poll_reregister_fd(poll, fd.as_raw_fd(), tok, can_send, can_recv);
    }

    fn get_tcp_conn(
        &mut self,
        sockets: &mut SocketSet,
        src_addr: IpAddress,
        dst_addr: IpAddress,
        tcp_repr: &TcpRepr,
    ) -> std::io::Result<(&mut SlirpTcp, usize)> {
        let e = self.tcp_src_map.entry((src_addr, tcp_repr.src_port));
        let conn = match e {
            Vacant(e) => {
                use net2::{TcpBuilder, TcpStreamExt};

                debug!("new TCP sock");
                let tcp = TcpBuilder::new_v4()?;
                let tcp = tcp.to_tcp_stream()?;
                tcp.set_nonblocking(true)?;
                tcp.connect((std::net::IpAddr::from(dst_addr), tcp_repr.dst_port))
                    .or_else(|err| {
                        if err.raw_os_error() == Some(libc::EINPROGRESS) {
                            Ok(())
                        } else {
                            Err(err)
                        }
                    })?;

                let tcp_rx_buffer = TcpSocketBuffer::new(vec![0; 65535]);
                let tcp_tx_buffer = TcpSocketBuffer::new(vec![0; 65535]);
                let mut tcp_socket = TcpSocket::new(tcp_rx_buffer, tcp_tx_buffer);
                tcp_socket.listen(tcp_repr.dst_port).unwrap(); // so next accept is for this socket

                let handle = sockets.add(tcp_socket);

                // FIXME: add timeout
                let tcp = SlirpTcp {
                    socket: tcp,
                    src: (src_addr, tcp_repr.src_port),
                    dst: (dst_addr, tcp_repr.dst_port),
                    handle,
                };

                let fd = tcp.socket.as_raw_fd();
                let tok = self.conns.insert(SlirpConn::Tcp(tcp));
                self.poll
                    .register(
                        &EventedFd(&fd),
                        mio::Token(tok),
                        mio::Ready::empty() | mio::unix::UnixReady::hup(),
                        mio::PollOpt::level(),
                    )
                    .unwrap();
                e.insert(tok);
                (&mut self.conns[tok], tok)
            }
            Occupied(e) => (&mut self.conns[*e.get()], *e.get()),
        };

        Ok((conn.0.as_mut_tcp(), conn.1))
    }

    fn process_tcp<'frame>(
        &mut self,
        sockets: &mut SocketSet,
        ip_repr: IpRepr,
        timestamp: Instant,
        ip_payload: &'frame [u8],
    ) -> Result<Packet<'frame>> {
        let (src_addr, dst_addr) = (ip_repr.src_addr(), ip_repr.dst_addr());
        let tcp_packet = TcpPacket::new_checked(ip_payload)?;
        let checksum_caps = self.device_capabilities.checksum.clone();
        let tcp_repr = TcpRepr::parse(&tcp_packet, &src_addr, &dst_addr, &checksum_caps)?;

        let mut reply = None;
        let mut rere = None;
        match self.get_tcp_conn(sockets, src_addr, dst_addr, &tcp_repr) {
            Ok((tcp, tok)) => {
                let mut tcp_socket = sockets.get::<TcpSocket>(tcp.handle);

                if tcp_socket.is_listening() {
                    if let Ok(Some(err)) = tcp.socket.take_error() {
                        if err.raw_os_error() == Some(libc::EINPROGRESS) {
                            debug!("Still connecting, we should queue incoming packets instead?");
                        } else {
                            tcp_socket.close();
                        }
                    }
                }

                if tcp_socket.accepts(&ip_repr, &tcp_repr) {
                    //let old_state = tcp_socket.state();

                    match tcp_socket.process(timestamp, &ip_repr, &tcp_repr) {
                        // The packet is valid and handled by socket.
                        Ok(rep) => {
                            match tcp_socket.state() {
                                TcpState::Established // if old_state != TcpState::Established
                                    => {
                                    rere = Some((
                                        tcp_socket.can_send(),
                                        tcp_socket.can_recv(),
                                        tcp.socket.as_raw_fd(),
                                        tok,
                                    ));
                                }
                                TcpState::CloseWait => {
                                    if let Err(e) = tcp.socket.shutdown(std::net::Shutdown::Write) {
                                        debug!("Failed to shutdown(): {}", e);
                                    }
                                    rere = Some((
                                        tcp_socket.can_send(),
                                        false,
                                        tcp.socket.as_raw_fd(),
                                        tok,
                                    ));
                                }
                                _ => {
                                    dbg!(tcp_socket.state());
                                }
                            };

                            let pkt = rep.map_or(Packet::None, Packet::Tcp);
                            reply = Some(Ok(pkt));
                        }
                        // The packet is malformed, or doesn't match the socket state,
                        // or the socket buffer is full.
                        Err(e) => {
                            reply = Some(Err(e));
                        }
                    }
                }
            }
            Err(e) => {
                dbg!(e);
            }
        }

        if let Some((can_send, can_recv, fd, tok)) = rere {
            poll_reregister_fd(&self.poll, fd, tok, can_send, can_recv);
        }

        if let Some(reply) = reply {
            return reply;
        } else if tcp_repr.control == TcpControl::Rst {
            // Never reply to a TCP RST packet with another TCP RST packet.
            return Ok(Packet::None);
        } else {
            // The packet wasn't handled by a socket, send a TCP RST packet.
            return Ok(Packet::Tcp(TcpSocket::rst_reply(&ip_repr, &tcp_repr)));
        }
    }

    fn process_ipv4<'frame, T: AsRef<[u8]>>(
        &mut self,
        sockets: &mut SocketSet,
        timestamp: Instant,
        eth_frame: &EthernetFrame<&'frame T>,
    ) -> Result<Packet<'frame>> {
        let ipv4_packet = Ipv4Packet::new_checked(eth_frame.payload())?;
        let checksum_caps = self.device_capabilities.checksum.clone();
        let ipv4_repr = Ipv4Repr::parse(&ipv4_packet, &checksum_caps)?;

        let ip_repr = IpRepr::Ipv4(ipv4_repr);
        let ip_payload = ipv4_packet.payload();

        match ipv4_repr.protocol {
            IpProtocol::Udp => self.process_udp(ip_repr, ip_payload),
            IpProtocol::Tcp => self.process_tcp(sockets, ip_repr, timestamp, ip_payload),
            _ => {
                // Send back as much of the original payload as we can.
                let payload_len =
                    icmp_reply_payload_len(ip_payload.len(), IPV4_MIN_MTU, ipv4_repr.buffer_len());
                let icmp_reply_repr = Icmpv4Repr::DstUnreachable {
                    reason: Icmpv4DstUnreachable::ProtoUnreachable,
                    header: ipv4_repr,
                    data: &ip_payload[0..payload_len],
                };
                Ok(self.icmpv4_reply(ipv4_repr, icmp_reply_repr))
            }
        }
    }

    fn process_ethernet<'frame, T: AsRef<[u8]>>(
        &mut self,
        sockets: &mut SocketSet,
        timestamp: Instant,
        frame: &'frame T,
    ) -> Result<Packet<'frame>> {
        let eth_frame = EthernetFrame::new_checked(frame)?;

        match eth_frame.ethertype() {
            EthernetProtocol::Arp => self.process_arp(timestamp, &eth_frame),
            EthernetProtocol::Ipv4 => self.process_ipv4(sockets, timestamp, &eth_frame),
            _ => Err(Error::Unrecognized),
        }
    }

    fn dispatch_ethernet<Tx, F>(
        &self,
        tx_token: Tx,
        timestamp: Instant,
        buffer_len: usize,
        f: F,
    ) -> Result<()>
    where
        Tx: TxToken,
        F: FnOnce(EthernetFrame<&mut [u8]>),
    {
        let tx_len = EthernetFrame::<&[u8]>::buffer_len(buffer_len);
        tx_token.consume(timestamp, tx_len, |tx_buffer| {
            debug_assert!(tx_buffer.as_ref().len() == tx_len);
            let mut frame = EthernetFrame::new_unchecked(tx_buffer.as_mut());
            frame.set_src_addr(self.special_ethernet_addr);

            f(frame);

            Ok(())
        })
    }

    fn has_neighbor<'a>(&self, addr: &'a IpAddress, timestamp: Instant) -> bool {
        self.neighbor_cache.lookup_pure(addr, timestamp).is_some()
    }

    fn lookup_hardware_addr<Tx>(
        &mut self,
        tx_token: Tx,
        timestamp: Instant,
        src_addr: &IpAddress,
        dst_addr: &IpAddress,
    ) -> Result<(EthernetAddress, Tx)>
    where
        Tx: TxToken,
    {
        if dst_addr.is_multicast() {
            let b = dst_addr.as_bytes();
            let hardware_addr = match dst_addr {
                &IpAddress::Unspecified => None,
                &IpAddress::Ipv4(_addr) => Some(EthernetAddress::from_bytes(&[
                    0x01,
                    0x00,
                    0x5e,
                    b[1] & 0x7F,
                    b[2],
                    b[3],
                ])),
                &IpAddress::Ipv6(_addr) => Some(EthernetAddress::from_bytes(&[
                    0x33, 0x33, b[12], b[13], b[14], b[15],
                ])),
                &IpAddress::__Nonexhaustive => unreachable!(),
            };
            match hardware_addr {
                Some(hardware_addr) =>
                // Destination is multicast
                {
                    return Ok((hardware_addr, tx_token))
                }
                None =>
                // Continue
                {
                    ()
                }
            }
        }

        match self.neighbor_cache.lookup(&dst_addr, timestamp) {
            NeighborAnswer::Found(hardware_addr) => return Ok((hardware_addr, tx_token)),
            NeighborAnswer::RateLimited => return Err(Error::Unaddressable),
            NeighborAnswer::NotFound => (),
        }

        match (src_addr, *dst_addr) {
            (&IpAddress::Ipv4(src_addr), IpAddress::Ipv4(dst_addr)) => {
                debug!(
                    "address {} not in neighbor cache, sending ARP request",
                    dst_addr
                );

                let arp_repr = ArpRepr::EthernetIpv4 {
                    operation: ArpOperation::Request,
                    source_hardware_addr: self.special_ethernet_addr,
                    source_protocol_addr: src_addr,
                    target_hardware_addr: EthernetAddress::BROADCAST,
                    target_protocol_addr: dst_addr,
                };

                self.dispatch_ethernet(tx_token, timestamp, arp_repr.buffer_len(), |mut frame| {
                    frame.set_dst_addr(EthernetAddress::BROADCAST);
                    frame.set_ethertype(EthernetProtocol::Arp);

                    arp_repr.emit(&mut ArpPacket::new_unchecked(frame.payload_mut()))
                })?;

                Err(Error::Unaddressable)
            }

            (&IpAddress::Ipv6(src_addr), IpAddress::Ipv6(dst_addr)) => {
                debug!(
                    "address {} not in neighbor cache, sending Neighbor Solicitation",
                    dst_addr
                );

                let checksum_caps = self.device_capabilities.checksum.clone();

                let solicit = Icmpv6Repr::Ndisc(NdiscRepr::NeighborSolicit {
                    target_addr: src_addr,
                    lladdr: Some(self.special_ethernet_addr),
                });

                let ip_repr = IpRepr::Ipv6(Ipv6Repr {
                    src_addr: src_addr,
                    dst_addr: dst_addr.solicited_node(),
                    next_header: IpProtocol::Icmpv6,
                    payload_len: solicit.buffer_len(),
                    hop_limit: 0xff,
                });

                self.dispatch_ip(tx_token, timestamp, ip_repr, |ip_repr, payload| {
                    solicit.emit(
                        &ip_repr.src_addr(),
                        &ip_repr.dst_addr(),
                        &mut Icmpv6Packet::new_unchecked(payload),
                        &checksum_caps,
                    );
                })?;

                Err(Error::Unaddressable)
            }

            _ => Err(Error::Unaddressable),
        }
    }

    fn dispatch_ip<Tx, F>(
        &mut self,
        tx_token: Tx,
        timestamp: Instant,
        ip_repr: IpRepr,
        f: F,
    ) -> Result<()>
    where
        Tx: TxToken,
        F: FnOnce(IpRepr, &mut [u8]),
    {
        let ip_repr = ip_repr.lower(&self.ip_addrs)?;
        let checksum_caps = self.device_capabilities.checksum.clone();

        let (dst_hardware_addr, tx_token) = self.lookup_hardware_addr(
            tx_token,
            timestamp,
            &ip_repr.src_addr(),
            &ip_repr.dst_addr(),
        )?;

        self.dispatch_ethernet(tx_token, timestamp, ip_repr.total_len(), |mut frame| {
            frame.set_dst_addr(dst_hardware_addr);
            match ip_repr {
                IpRepr::Ipv4(_) => frame.set_ethertype(EthernetProtocol::Ipv4),
                IpRepr::Ipv6(_) => frame.set_ethertype(EthernetProtocol::Ipv6),
                _ => return,
            }

            ip_repr.emit(frame.payload_mut(), &checksum_caps);

            let payload = &mut frame.payload_mut()[ip_repr.buffer_len()..];
            f(ip_repr, payload)
        })
    }

    fn dispatch<Tx>(&mut self, tx_token: Tx, timestamp: Instant, packet: Packet) -> Result<()>
    where
        Tx: TxToken,
    {
        let checksum_caps = self.device_capabilities.checksum.clone();
        match packet {
            Packet::Arp(arp_repr) => {
                let dst_hardware_addr = match arp_repr {
                    ArpRepr::EthernetIpv4 {
                        target_hardware_addr,
                        ..
                    } => target_hardware_addr,
                    _ => unreachable!(),
                };

                self.dispatch_ethernet(tx_token, timestamp, arp_repr.buffer_len(), |mut frame| {
                    frame.set_dst_addr(dst_hardware_addr);
                    frame.set_ethertype(EthernetProtocol::Arp);

                    let mut packet = ArpPacket::new_unchecked(frame.payload_mut());
                    arp_repr.emit(&mut packet);
                })
            }
            Packet::Icmpv4((ipv4_repr, icmpv4_repr)) => self.dispatch_ip(
                tx_token,
                timestamp,
                IpRepr::Ipv4(ipv4_repr),
                |_ip_repr, payload| {
                    icmpv4_repr.emit(&mut Icmpv4Packet::new_unchecked(payload), &checksum_caps);
                },
            ),
            Packet::Icmpv6((ipv6_repr, icmpv6_repr)) => self.dispatch_ip(
                tx_token,
                timestamp,
                IpRepr::Ipv6(ipv6_repr),
                |ip_repr, payload| {
                    icmpv6_repr.emit(
                        &ip_repr.src_addr(),
                        &ip_repr.dst_addr(),
                        &mut Icmpv6Packet::new_unchecked(payload),
                        &checksum_caps,
                    );
                },
            ),
            Packet::Dhcpv4((ip_repr, dhcp_repr)) => {
                let mut bytes = vec![0xa5; dhcp_repr.buffer_len()];
                let mut dhcp_packet = DhcpPacket::new_unchecked(&mut bytes);
                dhcp_repr.emit(&mut dhcp_packet).unwrap();

                let udp_repr = UdpRepr {
                    src_port: 67,
                    dst_port: 68,
                    payload: dhcp_packet.into_inner(),
                };
                let ip_repr = Ipv4Repr {
                    payload_len: udp_repr.buffer_len(),
                    ..ip_repr
                };
                self.dispatch(
                    tx_token,
                    timestamp,
                    Packet::Udp((IpRepr::Ipv4(ip_repr), udp_repr)),
                )
            }
            Packet::Udp((ip_repr, udp_repr)) => {
                self.dispatch_ip(tx_token, timestamp, ip_repr, |ip_repr, payload| {
                    udp_repr.emit(
                        &mut UdpPacket::new_unchecked(payload),
                        &ip_repr.src_addr(),
                        &ip_repr.dst_addr(),
                        &checksum_caps,
                    );
                })
            }
            Packet::Tcp((ip_repr, mut tcp_repr)) => {
                let caps = self.device_capabilities.clone();
                self.dispatch_ip(tx_token, timestamp, ip_repr, |ip_repr, payload| {
                    // copied directly from ethernet.rs:
                    // This is a terrible hack to make TCP performance more acceptable on systems
                    // where the TCP buffers are significantly larger than network buffers,
                    // e.g. a 64 kB TCP receive buffer (and so, when empty, a 64k window)
                    // together with four 1500 B Ethernet receive buffers. If left untreated,
                    // this would result in our peer pushing our window and sever packet loss.
                    //
                    // I'm really not happy about this "solution" but I don't know what else to do.
                    if let Some(max_burst_size) = caps.max_burst_size {
                        let mut max_segment_size = caps.max_transmission_unit;
                        max_segment_size -= EthernetFrame::<&[u8]>::header_len();
                        max_segment_size -= ip_repr.buffer_len();
                        max_segment_size -= tcp_repr.header_len();

                        let max_window_size = max_burst_size * max_segment_size;
                        if tcp_repr.window_len as usize > max_window_size {
                            tcp_repr.window_len = max_window_size as u16;
                        }
                    }

                    tcp_repr.emit(
                        &mut TcpPacket::new_unchecked(payload),
                        &ip_repr.src_addr(),
                        &ip_repr.dst_addr(),
                        &checksum_caps,
                    );
                })
            }
            Packet::None => Ok(()),
        }
    }

    fn dhcpv4_reply<'frame, 'dhcp: 'frame>(
        &self,
        ipv4_repr: Ipv4Repr,
        dhcp_repr: DhcpRepr<'dhcp>,
    ) -> Packet<'frame> {
        if ipv4_repr.src_addr.is_unspecified() {
            let ipv4_reply_repr = Ipv4Repr {
                src_addr: dhcp_repr.server_ip,
                dst_addr: Ipv4Address::BROADCAST,
                protocol: IpProtocol::Udp,
                payload_len: 0,
                hop_limit: 64,
            };
            Packet::Dhcpv4((ipv4_reply_repr, dhcp_repr))
        } else {
            debug!("42");
            Packet::None
        }
    }

    fn icmpv4_reply<'frame, 'icmp: 'frame>(
        &self,
        ipv4_repr: Ipv4Repr,
        icmp_repr: Icmpv4Repr<'icmp>,
    ) -> Packet<'frame> {
        if ipv4_repr.dst_addr.is_unicast() {
            let ipv4_reply_repr = Ipv4Repr {
                src_addr: ipv4_repr.dst_addr,
                dst_addr: ipv4_repr.src_addr,
                protocol: IpProtocol::Icmp,
                payload_len: icmp_repr.buffer_len(),
                hop_limit: 64,
            };
            Packet::Icmpv4((ipv4_reply_repr, icmp_repr))
        } else {
            // Do not send any ICMP replies to a broadcast destination address.
            Packet::None
        }
    }

    fn icmpv6_reply<'frame, 'icmp: 'frame>(
        &self,
        ipv6_repr: Ipv6Repr,
        icmp_repr: Icmpv6Repr<'icmp>,
    ) -> Packet<'frame> {
        if ipv6_repr.dst_addr.is_unicast() {
            let ipv6_reply_repr = Ipv6Repr {
                src_addr: ipv6_repr.dst_addr,
                dst_addr: ipv6_repr.src_addr,
                next_header: IpProtocol::Icmpv6,
                payload_len: icmp_repr.buffer_len(),
                hop_limit: 64,
            };
            Packet::Icmpv6((ipv6_reply_repr, icmp_repr))
        } else {
            // Do not send any ICMP replies to a broadcast destination address.
            Packet::None
        }
    }
}

pub struct Slirp<'a, 'b, 'c, 'd, DeviceT: for<'z> Device<'z>> {
    inner: SlirpInner<'d>,
    sockets: SocketSet<'a, 'b, 'c>,
    device: DeviceT,
    fd: RawFd,
}

const FD_TOKEN: mio::Token = mio::Token(10_000_000);

impl<DeviceT> Slirp<'_, '_, '_, '_, DeviceT>
where
    DeviceT: for<'z> Device<'z>,
{
    pub fn new<T: Into<IpAddress>>(device: DeviceT, addr: T, fd: RawFd) -> Self {
        let addr = addr.into();
        let poll = Poll::new().unwrap();

        poll.register(
            &EventedFd(&fd),
            FD_TOKEN,
            mio::Ready::readable(),
            mio::PollOpt::level(),
        )
        .unwrap();

        let device_capabilities = device.capabilities();

        let ip_addrs = [IpCidr::new(addr, 24)];
        let mut special_ethernet_addr = EthernetAddress([0x52, 0x55, 0x0, 0x0, 0x0, 0x0]);
        special_ethernet_addr.0[2..].copy_from_slice(addr.as_bytes());

        Self {
            inner: SlirpInner {
                poll,
                conns: Slab::with_capacity(1024),
                ip_addrs: ip_addrs.into(),
                fwd_dns: None,
                neighbor_cache: NeighborCache::new(BTreeMap::new()),
                device_capabilities,
                special_ethernet_addr,
                udp_src_map: HashMap::new(),
                tcp_src_map: HashMap::new(),
            },
            sockets: SocketSet::new(vec![]),
            device,
            fd,
        }
    }

    pub fn set_fwd_dns<T: Into<IpAddress>>(&mut self, addr: T) -> Result<()> {
        let addr = addr.into();
        self.inner.fwd_dns = Some(addr);
        Ok(())
    }

    fn device_ingress(&mut self, timestamp: Instant) -> Result<bool> {
        let mut processed_any = false;
        loop {
            let &mut Self {
                ref mut inner,
                ref mut device,
                ref mut sockets,
                ..
            } = self;
            let (rx_token, tx_token) = match device.receive() {
                None => break,
                Some(tokens) => tokens,
            };

            rx_token.consume(timestamp, |frame| {
                inner
                    .process_ethernet(sockets, timestamp, &frame)
                    .map_err(|err| {
                        debug!("cannot process ingress packet: {}", err);
                        debug!(
                            "packet dump follows:\n{}",
                            PrettyPrinter::<EthernetFrame<&[u8]>>::new("", &frame)
                        );
                        err
                    })
                    .and_then(|response| {
                        processed_any = true;
                        inner
                            .dispatch(tx_token, timestamp, response)
                            .map_err(|err| {
                                debug!("cannot dispatch response packet: {}", err);
                                err
                            })
                    })
            })?;
        }

        Ok(processed_any)
    }

    fn device_egress(&mut self, timestamp: Instant) -> Result<bool> {
        let mut caps = self.device.capabilities();
        caps.max_transmission_unit -= EthernetFrame::<&[u8]>::header_len();

        let &mut Self {
            ref mut device,
            ref mut inner,
            ref mut sockets,
            ..
        } = self;

        // FIXME: is there fairness between sockets?
        let mut emitted_any = false;
        for mut socket in sockets.iter_mut() {
            if !socket
                .meta_mut()
                .egress_permitted(|ip_addr| inner.has_neighbor(&ip_addr, timestamp))
            {
                debug!("not permitted");
                continue;
            }

            let mut neighbor_addr = None;
            let mut device_result = Ok(());

            macro_rules! respond {
                ($response:expr) => {{
                    let response = $response;
                    neighbor_addr = response.neighbor_addr();
                    let tx_token = device.transmit().ok_or(Error::Exhausted)?;
                    device_result = inner.dispatch(tx_token, timestamp, response);
                    device_result
                }};
            }

            let result = match *socket {
                Socket::Tcp(ref mut socket) => {
                    socket.dispatch(timestamp, &caps, |response| respond!(Packet::Tcp(response)))
                }
                Socket::Udp(ref mut socket) => {
                    socket.dispatch(|response| respond!(Packet::Udp(response)))
                }
                _ => unimplemented!(),
            };

            match (device_result, result) {
                (Err(Error::Exhausted), _) => break,   // nowhere to transmit
                (Ok(()), Err(Error::Exhausted)) => (), // nothing to transmit
                (Err(Error::Unaddressable), _) => {
                    // `NeighborCache` already takes care of rate limiting the neighbor discovery
                    // requests from the socket. However, without an additional rate limiting
                    // mechanism, we would spin on every socket that has yet to discover its
                    // neighboor.
                    debug!("neighbor_missing");
                    break;
                }
                (Err(err), _) | (_, Err(err)) => {
                    debug!("cannot dispatch packet: {}", err);
                    return Err(err);
                }
                (Ok(()), Ok(())) => emitted_any = true,
            }
        }

        Ok(emitted_any)
    }

    pub fn conn_readable(&mut self, token: &mio::Token, timestamp: Instant) -> Result<()> {
        let &mut Self {
            ref mut device,
            ref mut inner,
            ref mut sockets,
            ..
        } = self;
        let conn = &inner.conns[token.0];
        let mut neighbor_addr = None;
        let mut device_result = Ok(());

        macro_rules! respond {
            ($inner:expr, $response:expr) => {{
                let response = $response;
                let inner = $inner;
                neighbor_addr = response.neighbor_addr();
                let tx_token = device.transmit().ok_or(Error::Exhausted)?;
                device_result = inner.dispatch(tx_token, timestamp, response);
                device_result
            }};
        }

        match conn {
            SlirpConn::Udp(_) => inner.conn_readable_udp(token.0, |inn, response| {
                respond!(inn, Packet::Udp(response))
            }),
            SlirpConn::Tcp(_) => inner.conn_readable_tcp(sockets, token.0),
        }
    }

    fn conn_writable(&mut self, token: &mio::Token) -> Result<()> {
        let &mut Self {
            ref mut inner,
            ref mut sockets,
            ..
        } = self;
        let conn = &inner.conns[token.0];

        match conn {
            SlirpConn::Tcp(_) => inner.conn_writable_tcp(sockets, token.0),
            _ => unimplemented!(),
        }
    }

    fn conn_hup(&mut self, token: &mio::Token) -> Result<()> {
        let &mut Self {
            ref mut inner,
            ref mut sockets,
            ..
        } = self;
        let conn = &inner.conns[token.0];

        match conn {
            SlirpConn::Tcp(_) => inner.conn_hup_tcp(sockets, token.0),
            _ => unimplemented!(),
        }
    }

    fn poll_at(&self, timestamp: Instant) -> Option<Instant> {
        self.sockets
            .iter()
            .filter_map(|socket| {
                let socket_poll_at = socket.poll_at();
                match socket.meta().poll_at(socket_poll_at, |ip_addr| {
                    self.inner.has_neighbor(&ip_addr, timestamp)
                }) {
                    PollAt::Ingress => None,
                    PollAt::Time(instant) => Some(instant),
                    PollAt::Now => Some(Instant::from_millis(0)),
                }
            })
            .min()
    }

    fn poll_delay(&self, timestamp: Instant) -> Option<Duration> {
        match self.poll_at(timestamp) {
            Some(poll_at) if timestamp < poll_at => Some(poll_at - timestamp),
            Some(_) => Some(Duration::from_millis(0)),
            _ => None,
        }
    }

    fn cleanup_closed(&mut self) {
        let mut closed = vec![];
        for (tok, conn) in self.inner.conns.iter() {
            if let SlirpConn::Tcp(tcp) = conn {
                let socket = self.sockets.get::<TcpSocket>(tcp.handle);
                if socket.state() == TcpState::Closed {
                    closed.push(tok);
                }
            }
        }

        for closed in &closed {
            let conn = self.inner.conns.remove(*closed);
            if let SlirpConn::Tcp(tcp) = conn {
                self.sockets.remove(tcp.handle);
                self.inner.tcp_src_map.remove(&(tcp.src.0, tcp.src.1));
            }
        }
    }

    pub fn poll(
        &mut self,
        duration: Option<std::time::Duration>,
    ) -> Result<Option<std::time::Duration>> {
        //dbg!("poll");
        let mut events = Events::with_capacity(1024);
        self.inner.poll.poll(&mut events, duration).unwrap();

        let timestamp = Instant::now();
        for event in events {
            //dbg!(&event);
            if mio::unix::UnixReady::from(event.readiness()).is_hup()
                || mio::unix::UnixReady::from(event.readiness()).is_error()
            {
                match event.token() {
                    t if t.0 < self.inner.conns.capacity() => self.conn_hup(&t)?,
                    _ => panic!(),
                }
                continue;
            }

            if event.readiness().is_readable() {
                match event.token() {
                    FD_TOKEN => self.device_ingress(timestamp)?,
                    t if t.0 < self.inner.conns.capacity() => {
                        self.conn_readable(&t, timestamp).map(|_| true)?
                    }
                    _ => panic!(),
                };
            }
            if event.readiness().is_writable() {
                match event.token() {
                    FD_TOKEN => self.device_egress(timestamp)?,
                    t if t.0 < self.inner.conns.capacity() => {
                        self.conn_writable(&t).map(|_| true)?
                    }
                    _ => panic!(),
                };
            }
        }

        loop {
            let ingress_any = self.device_ingress(timestamp)?;
            let egress_any = self.device_egress(timestamp)?;

            if !ingress_any && !egress_any {
                break;
            }
        }

        self.cleanup_closed();

        let new_delay = self.poll_delay(timestamp).map(|d| d.into());
        let mut cond = mio::Ready::readable();
        if let Some(d) = new_delay {
            if d == std::time::Duration::new(0, 0) {
                cond |= mio::Ready::writable()
            }
        }

        self.inner
            .poll
            .reregister(&EventedFd(&self.fd), FD_TOKEN, cond, mio::PollOpt::level())
            .unwrap();

        Ok(new_delay)
    }
}

#[cfg(test)]
mod test {
    use super::Slirp;
    use crate::dgram;
    use smoltcp::iface::{EthernetInterfaceBuilder, NeighborCache, Routes};
    use smoltcp::phy::DeviceCapabilities;
    use smoltcp::socket::{SocketSet, TcpSocket, TcpSocketBuffer};
    use smoltcp::time::Instant;
    use smoltcp::wire::{EthernetAddress, IpAddress, IpCidr, Ipv4Address};
    use std::collections::BTreeMap;
    use std::net::{Ipv4Addr, TcpListener};
    use std::os::unix::io::AsRawFd;
    use std::os::unix::net::UnixDatagram;
    use std::thread;
    use std::time::Duration;

    fn socket_with_buffer_sizes(tx_len: usize, rx_len: usize) -> TcpSocket<'static> {
        let rx_buffer = TcpSocketBuffer::new(vec![0; rx_len]);
        let tx_buffer = TcpSocketBuffer::new(vec![0; tx_len]);
        TcpSocket::new(rx_buffer, tx_buffer)
    }

    fn socket() -> TcpSocket<'static> {
        socket_with_buffer_sizes(64, 64)
    }

    fn duration0() -> Option<Duration> {
        Some(Duration::new(0, 0))
    }

    #[test]
    pub fn test_one() {
        let (sock1, sock2) = match UnixDatagram::pair() {
            Ok((sock1, sock2)) => (sock1, sock2),
            Err(e) => {
                println!("Couldn't unbound: {:?}", e);
                return;
            }
        };

        let fd = sock2.as_raw_fd();
        let device2 = dgram::DgramDevice::new(sock2);
        let addr: Ipv4Addr = "10.0.2.2".parse().unwrap();
        let mut slirp = Slirp::new(device2, addr, fd);

        let mut duration = slirp.poll(duration0()).unwrap();
        assert_eq!(duration, None);

        let listener = TcpListener::bind("127.0.0.1:0").unwrap();
        let listener_addr = listener.local_addr().unwrap();
        let server_addr = (listener_addr.ip(), listener_addr.port());

        thread::spawn(move || {
            for stream in listener.incoming() {
                match stream {
                    Ok(stream) => {
                        println!("New connection: {}", stream.peer_addr().unwrap());
                        // connection succeeded
                    }
                    Err(e) => {
                        println!("Error: {}", e);
                        /* connection failed */
                    }
                }
            }
        });

        let neighbor_cache = NeighborCache::new(BTreeMap::new());
        let device1 = dgram::DgramDevice::new(sock1);

        let tcp_rx_buffer = TcpSocketBuffer::new(vec![0; 64]);
        let tcp_tx_buffer = TcpSocketBuffer::new(vec![0; 64]);
        let tcp_socket = TcpSocket::new(tcp_rx_buffer, tcp_tx_buffer);

        let ethernet_addr = EthernetAddress([0x02, 0x00, 0x00, 0x00, 0x00, 0x02]);
        let ip_addrs = [IpCidr::new(IpAddress::v4(10, 0, 2, 42), 24)];
        let default_v4_gw = Ipv4Address::new(10, 0, 2, 2);
        let mut routes_storage = [None; 1];
        let mut routes = Routes::new(&mut routes_storage[..]);
        routes.add_default_ipv4_route(default_v4_gw).unwrap();
        let mut iface = EthernetInterfaceBuilder::new(device1)
            .ethernet_addr(ethernet_addr)
            .neighbor_cache(neighbor_cache)
            .ip_addrs(ip_addrs)
            .routes(routes)
            .finalize();

        let mut sockets = SocketSet::new(vec![]);
        let tcp_handle = sockets.add(tcp_socket);

        {
            let mut socket = sockets.get::<TcpSocket>(tcp_handle);
            socket.connect(server_addr, 49500).unwrap();
        }

        loop {
            //dbg!("loo");
            let timestamp = Instant::now();
            iface.poll(&mut sockets, timestamp).unwrap();
            duration = slirp.poll(duration0()).unwrap();
            //dbg!(duration);
            let mut socket = sockets.get::<TcpSocket>(tcp_handle);
            if socket.may_send() && !socket.may_recv() {
                dbg!("he");
                socket.close();
            }
        }
    }
}
