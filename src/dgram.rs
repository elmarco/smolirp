use smoltcp::phy;
use smoltcp::time::Instant;
use smoltcp::Result;
use std::cell::RefCell;
use std::io;
use std::os::unix::net::UnixDatagram;
use std::rc::Rc;

pub struct DgramDesc {
    dgram: UnixDatagram,
    mtu: usize,
}

impl DgramDesc {
    fn new(dgram: UnixDatagram, mtu: usize) -> Self {
        dgram.set_nonblocking(true).unwrap();
        Self { dgram, mtu }
    }

    fn recv(&mut self) -> io::Result<Vec<u8>> {
        let mut buffer = vec![0; self.mtu];

        self.dgram.recv(&mut buffer[..]).and_then(|size| {
            buffer.resize(size, 0);
            Ok(buffer)
        })
    }
}

pub struct DgramDevice {
    pub lower: Rc<RefCell<DgramDesc>>,
}

impl DgramDevice {
    pub fn new(dgram: UnixDatagram) -> DgramDevice {
        let mtu = 4096 + 65536; // FIXME: defined by qemu
        Self {
            lower: Rc::new(RefCell::new(DgramDesc::new(dgram, mtu))),
        }
    }
}

pub struct DgramRxToken {
    buffer: Vec<u8>,
}

impl<'a> phy::RxToken for DgramRxToken {
    fn consume<R, F>(mut self, _timestamp: Instant, f: F) -> Result<R>
    where
        F: FnOnce(&mut [u8]) -> Result<R>,
    {
        f(&mut self.buffer)
    }
}

pub struct DgramTxToken {
    lower: Rc<RefCell<DgramDesc>>,
}

impl phy::TxToken for DgramTxToken {
    fn consume<R, F>(self, _timestamp: Instant, len: usize, f: F) -> Result<R>
    where
        F: FnOnce(&mut [u8]) -> Result<R>,
    {
        let mut buffer = vec![0; len];
        let result = f(&mut buffer); // TxToken could be replaced by a Write impl?
        let lower = self.lower.borrow();

        // FIXME: stay nonblocking and assert(sent == len)?
        // /!\ should only be called when socket is POLLOUT to avoid blocking
        lower.dgram.set_nonblocking(false).unwrap();
        lower.dgram.send(&buffer[..]).unwrap();
        lower.dgram.set_nonblocking(true).unwrap();
        result
    }
}

impl<'a> phy::Device<'a> for DgramDevice {
    type RxToken = DgramRxToken;
    type TxToken = DgramTxToken;

    fn capabilities(&self) -> phy::DeviceCapabilities {
        let mut caps = phy::DeviceCapabilities::default();
        caps.max_transmission_unit = self.lower.borrow().mtu;
        caps
    }

    fn receive(&'a mut self) -> Option<(Self::RxToken, Self::TxToken)> {
        let recv = self.lower.borrow_mut().recv();

        match recv {
            Ok(buffer) => {
                let rx = Self::RxToken { buffer };
                let tx = Self::TxToken {
                    lower: self.lower.clone(),
                };
                Some((rx, tx))
            }
            Err(ref err) if err.kind() == io::ErrorKind::WouldBlock => None,
            Err(err) => panic!("{}", err),
        }
    }

    fn transmit(&'a mut self) -> Option<Self::TxToken> {
        Some(Self::TxToken {
            lower: self.lower.clone(),
        })
    }
}
