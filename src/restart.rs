use std::env;

pub fn register() {
    let signal = unsafe {
        signal_hook::register(signal_hook::SIGUSR1, || {
            use std::process;
            use std::process::Command;

            debug!("got SIGUSR1, restarting");
            let argv: Vec<String> = env::args().collect();
            let _ = Command::new(&argv[0]).args(&argv[1..]).spawn();
            process::exit(0);
        })
    };
    signal.unwrap();
}
