use libc;
use std::os::unix::io::RawFd;

macro_rules! try_ioctl {
    ($fd:expr,$cmd:expr,$req:expr) => {
        unsafe {
            if libc::ioctl($fd, $cmd, $req) == -1 {
                return Err(std::io::Error::last_os_error());
            }
        }
    };
}

pub fn get_input_buffer_size(fd: RawFd) -> std::io::Result<usize> {
    let mut bufsz = 0 as libc::c_int;

    try_ioctl!(fd, libc::FIONREAD, &mut bufsz);

    Ok(bufsz as usize)
}
