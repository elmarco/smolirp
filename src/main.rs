#[macro_use]
extern crate log;

use std::net::Ipv4Addr;
use std::os::unix::io::FromRawFd;
use std::os::unix::net::UnixDatagram;

mod dgram;
mod ioctls;
mod restart;
mod slirp;
mod utils;

fn main() {
    utils::setup_logging("");

    let (mut opts, mut free) = utils::create_options();
    utils::add_middleware_options(&mut opts, &mut free);
    free.push("FD");
    let mut matches = utils::parse_options(&opts, free);

    let fd = matches.free.remove(0).parse::<i32>().unwrap();
    let stream = unsafe { UnixDatagram::from_raw_fd(fd) };
    let device = dgram::DgramDevice::new(stream);
    let device = utils::parse_middleware_options(&mut matches, device, /*loopback=*/ false);

    restart::register();

    let addr: Ipv4Addr = "10.0.2.2".parse().unwrap();
    let mut slirp = slirp::Slirp::new(device, addr, fd);

    let dns: Ipv4Addr = "8.8.8.8".parse().unwrap();
    slirp.set_fwd_dns(dns).unwrap();

    let mut duration = None;
    loop {
        match slirp.poll(duration) {
            Ok(d) => duration = d,
            Err(e) => debug!("poll error: {}", e),
        }
    }
}
